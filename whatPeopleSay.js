$(document).ready(function () {
    const profiles = [
        {
            name: "Hasan Ali",
            job: "UX Designer",
            imgLarge: "./img/What_People_Say/slider-img1.jpg",
            imgSmall: "./img/What_People_Say/slider-img1.jpg",
        },
        {
            name: "Anna Smith",
            job: "Graphic Designer",
            imgLarge: "./img/What_People_Say/slider-img2.jpg",
            imgSmall: "./img/What_People_Say/slider-img2.jpg",
        },
        {
            name: "John Doe",
            job: "Web Developer",
            imgLarge: "./img/What_People_Say/slider-img3.jpg",
            imgSmall: "./img/What_People_Say/slider-img3.jpg",
        },
        {
            name: "Emily Johnson",
            job: "Product Manager",
            imgLarge: "./img/What_People_Say/slider-img4.jpg",
            imgSmall: "./img/What_People_Say/slider-img4.jpg",
        },
    ];

    let currentIndex = 0;

    function updateProfile() {
        const currentProfile = profiles[currentIndex];
        $(".profile-name").text(currentProfile.name);
        $(".profile-job").text(currentProfile.job);
        $(".profile-img-one-large").attr("src", currentProfile.imgLarge);
    
        $(".profile-small img").css("margin-top", "5px");
    
        const currentSmallProfile = $(".profile-small img").eq(currentIndex);
        currentSmallProfile.css("margin-top", "-15px"); 
    
        $(".profile-small img, .profile-small").css("transition", "margin-top 0.3s ease");
    }

    $(".left-arrow-img").click(function () {
        currentIndex = (currentIndex - 1 + profiles.length) % profiles.length;
        updateProfile();
    });

    $(".right-arrow-img").click(function () {
        currentIndex = (currentIndex + 1) % profiles.length;
        updateProfile();
    });

    updateProfile();
});