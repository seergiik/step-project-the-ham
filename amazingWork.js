document.addEventListener("DOMContentLoaded", function () {
    const imageData = {
        all: [
            "./img/Our_amazing_Work/graphic_design_1.jpg",
            "./img/Our_amazing_Work/graphic_design_2.jpg",
            "./img/Our_amazing_Work/graphic_design_3.jpg",
            "./img/Our_amazing_Work/graphic_design_11.jpg",
            "./img/Our_amazing_Work/web_design_11.jpg",
            "./img/Our_amazing_Work/web_design_3.jpg",
            "./img/Our_amazing_Work/web_design_12.jpg",
            "./img/Our_amazing_Work/web_design_4.jpg",
            "./img/Our_amazing_Work/wordpress_4.jpg",
            "./img/Our_amazing_Work/wordpress_11.jpg",
            "./img/Our_amazing_Work/graphic_design_6.jpg",
             "./img/Our_amazing_Work/web_design_10.jpg",
            
        ],
        graphicDesign: [
            "./img/Our_amazing_Work/graphic_design_1.jpg",
            "./img/Our_amazing_Work/graphic_design_2.jpg",
            "./img/Our_amazing_Work/graphic_design_3.jpg",
            "./img/Our_amazing_Work/graphic_design_4.jpg",
            "./img/Our_amazing_Work/graphic_design_5.jpg",
            "./img/Our_amazing_Work/graphic_design_6.jpg",
            "./img/Our_amazing_Work/graphic_design_7.jpg",
            "./img/Our_amazing_Work/graphic_design_8.jpg",
            "./img/Our_amazing_Work/graphic_design_9.png",
            "./img/Our_amazing_Work/graphic_design_10.webp",
            "./img/Our_amazing_Work/graphic_design_11.jpg",
            "./img/Our_amazing_Work/graphic_design_12.avif",
        ],
        webDesign: [
            "./img/Our_amazing_Work/web_design_1.jpg",
            "./img/Our_amazing_Work/web_design_2.jpg",
            "./img/Our_amazing_Work/web_design_3.jpg",
            "./img/Our_amazing_Work/web_design_4.jpg",
            "./img/Our_amazing_Work/web_design_5.jpg",
            "./img/Our_amazing_Work/web_design_6.jpg",
            "./img/Our_amazing_Work/web_design_7.jpg",
            "./img/Our_amazing_Work/web_design_8.jpg",
            "./img/Our_amazing_Work/web_design_9.png",
            "./img/Our_amazing_Work/web_design_10.jpg",
            "./img/Our_amazing_Work/web_design_11.jpg",
            "./img/Our_amazing_Work/web_design_12.jpg",
            

        ],
        landingPages: [
            "./img/Our_amazing_Work/landing_page_1.avif",
            "./img/Our_amazing_Work/landing_page_2.avif",
            "./img/Our_amazing_Work/landing_page_3.png",
            "./img/Our_amazing_Work/landing_page_4.png",
            "./img/Our_amazing_Work/landing_page_5.jpg",
            "./img/Our_amazing_Work/landing_page_6.png",
            "./img/Our_amazing_Work/landing_page_7.png",
            "./img/Our_amazing_Work/landing_page_8.avif",
            "./img/Our_amazing_Work/landing_page_9.jpg",
            "./img/Our_amazing_Work/landing_page_10.jpg",
            "./img/Our_amazing_Work/landing_page_11.jpg",
            "./img/Our_amazing_Work/landing_page_12.png",
        ],
        wordpress: [
            "./img/Our_amazing_Work/wordpress_1.jpg",
            "./img/Our_amazing_Work/wordpress_2.jpg",
            "./img/Our_amazing_Work/wordpress_3.jpg",
            "./img/Our_amazing_Work/wordpress_4.jpg",
            "./img/Our_amazing_Work/wordpress_5.png",
            "./img/Our_amazing_Work/wordpress_6.png",
            "./img/Our_amazing_Work/wordpress_7.png",
            "./img/Our_amazing_Work/wordpress_8.jpg",
            "./img/Our_amazing_Work/wordpress_9.jpg",
            "./img/Our_amazing_Work/wordpress_10.jpg",
            "./img/Our_amazing_Work/wordpress_11.jpg",
            "./img/Our_amazing_Work/wordpress_12.webp",
        ],
        additional: [
            "./img/Our_amazing_Work/extra_1.avif",
            "./img/Our_amazing_Work/extra_2.jpg",
            "./img/Our_amazing_Work/extra_3.jpg",
            "./img/Our_amazing_Work/extra_4.avif",
            "./img/Our_amazing_Work/extra_5.jpg",
            "./img/Our_amazing_Work/extra_6.jpg",
            "./img/Our_amazing_Work/extra_7.webp",
            "./img/Our_amazing_Work/extra_8.jpg",
            "./img/Our_amazing_Work/extra_9.jpg",
            "./img/Our_amazing_Work/extra_10.jpg",
            "./img/Our_amazing_Work/extra_11.jpg",
            "./img/Our_amazing_Work/extra_12.jpg",
        ],
        
    };
    const tabs = document.querySelectorAll(".amazing-work-tabs-title");
    const imagesContainer = document.getElementById("amazing-work-images-container");
    const loadMoreBtn = document.querySelector('.load-more-btn');

    if (!imagesContainer || !loadMoreBtn || tabs.length === 0) {
        console.error("Error: Images container, Load More button, or tabs not found.");
        return;
    }

    let currentTab = "all";
    let originalAllData = [...imageData.all];

    function showImages(tab) {
        if (!imagesContainer) {
            console.error("Error: Images container not found.");
            return;
        }

        imagesContainer.innerHTML = "";

        const tabData = imageData[tab] || [];

        if (tabData.length === 0) {
            imagesContainer.innerHTML = "No images available for this category.";
            return;
        }

        for (let i = 0; i < tabData.length; i++) {
            const img = document.createElement("img");
            img.src = tabData[i];
            img.alt = `Image ${i + 1}`;
            img.className = `amazing-work-${tab}-tab`;

            img.style.width = "285px";
            img.style.height = "211px";

            imagesContainer.appendChild(img);
        }

        if (tab === "all" && imageData.additional.length > 0) {
            loadMoreBtn.style.display = "block";
        } else {
            loadMoreBtn.style.display = "none";
        }
    }

    tabs.forEach((tab) => {
        tab.addEventListener("click", () => {
            if (!tab.classList) {
                console.error("Error: ClassList not found for tab element.");
                return;
            }

            tabs.forEach((t) => t.classList && t.classList.remove("active"));
            tab.classList.add("active");
            const selectedTab = tab.getAttribute("data-tab");
            currentTab = selectedTab;
            showImages(selectedTab);
        });
    });

    const firstTab = tabs[0];
    if (firstTab && firstTab.click) {
        firstTab.click();
    } else {
        console.error("Error: Unable to click the first tab.");
    }

    loadMoreBtn.addEventListener('click', function () {
        const numberOfImagesToLoad = 12;

        if (!imagesContainer) {
            console.error("Error: Images container not found.");
            return;
        }

        originalAllData = [...imageData.all];

        const additionalImagesToLoad = imageData.additional.splice(0, numberOfImagesToLoad);
        imageData.all = originalAllData.concat(additionalImagesToLoad);
        showImages("all");
    });
});