document.addEventListener('DOMContentLoaded', function () {
    const tabTitles = document.querySelectorAll('.tabs-title');
    const tabContents = document.querySelectorAll('.tab-contnet');
    const activeTab = document.querySelector('.tabs-title.active');

    if (activeTab) {
        const activeTabId = activeTab.getAttribute('data-tab');
        const activeTabContent = document.getElementById(activeTabId);
        activeTabContent.style.display = 'block';
    }

    tabTitles.forEach((tabTitle) => {
        tabTitle.addEventListener('click', () => {
            tabTitles.forEach((title) => title.classList.remove('active'));
            tabTitle.classList.add('active');

            tabContents.forEach((content) => content.style.display = 'none');

            const tabId = tabTitle.getAttribute('data-tab');
            document.getElementById(tabId).style.display = 'block';
        });
    });
});